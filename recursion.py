#Script to recursively find and replace a substring
#KJames 22/08/2019 

print("Please enter a string: ") 
inp = input()
print("Please enter the substring you wish to find: ") 
sub = input()
print("Please enter a string to replace the given substring: ") 
rep = input()

def search(thestring):    
     if thestring == "" or len(sub) > len(thestring):
          return thestring
     if(thestring[0:len(sub)] == sub):           
          return rep + search(thestring[len(sub):])              
     return thestring[0] + search(thestring[1:])
     
print("Your new string is: {}".format(search(inp)))