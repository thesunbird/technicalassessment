import idx2numpy
import numpy as np
from sklearn.model_selection import cross_val_score, train_test_split
import cv2
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score, accuracy_score

np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)}) #print only to 3dp

def load_mnist(images,labels):
     x = idx2numpy.convert_from_file(images)
     y = idx2numpy.convert_from_file(labels)          
     return x,y 

def metric_report(test,preds):

     precision = precision_score(test,preds,average=None)
     recall = recall_score(test,preds,average=None)
     f1 = f1_score(test,preds,average=None)
     accuracy = accuracy_score(test,preds)

     print("Metrics")
     print("Precision: {} Average: {:.3f}".format(precision,np.mean(precision)))
     print("Recall: {} Average: {:.3f}".format(recall,np.mean(recall)))
     print("F1: {} Average: {:.3f}".format(f1, np.mean(f1)))
     print("Accuracy: {} \n".format(accuracy))
     #https://simonhessner.de/why-are-precision-recall-and-f1-score-equal-when-using-micro-averaging-in-a-multi-class-problem/

#Load dataset from disk
#load data using idx2numpy package - no point reinventing the wheel
#training set has len 60000, test set len 10000
path = ".\\samples\\"
X,y = load_mnist(path+"train-images-idx3-ubyte",path+"train-labels-idx1-ubyte")
X_test,y_test = load_mnist(path+"t10k-images-idx3-ubyte",path+"t10k-labels-idx1-ubyte")

assert len(X) == len(y)
assert len(X_test) == len(y_test)

#normalise data
X = X/255.
X_test = X_test/255.
  
#split train and val data - note there is already test data from the other file
X_train, X_val, y_train, y_val = train_test_split(X,y,test_size=0.30)

#reshape for sklearn
shp = X_train.shape
X_train = X_train.reshape(shp[0],shp[1]*shp[2])
shp = X_val.shape
X_val = X_val.reshape(shp[0],shp[1]*shp[2])
shp = X_test.shape
X_test = X_test.reshape(shp[0],shp[1]*shp[2])

#Model selection: decision tree and k-nearest-neighbour
#Tune max_depth parameter -- otherwise may overfit

print('Decision Tree')
print('Tuning max_depth')
best_d = 1
best_acc = 0

for d in range(1,20):
     tree = DecisionTreeClassifier(max_depth=d).fit(X_train,y_train)
     tree_preds = tree.predict(X_val)     
     accuracy = accuracy_score(y_val,tree_preds)
     print("{} {:.3f}".format(d,accuracy))
     if d==1:
          best_acc=accuracy
     elif accuracy- best_acc > 0.009: #keep going until accuracy is only changing at the third decimal place
          best_d = d
          best_acc = accuracy
     else:               
          break
          
print("Selected max_depth {} gives acc of {:.3f} on val: ".format(best_d,best_acc))        
print("Training final decision tree")
tree = DecisionTreeClassifier(max_depth=best_d).fit(X_train,y_train)
tree_preds = tree.predict(X_test)
tree_confusion = confusion_matrix(y_test,tree_preds)
print("Decision tree confusion matrix:")
print(tree_confusion)
metric_report(y_test,tree_preds)


#KNN is very slow. But quite good in terms of acc.
#tune k, the number of neighbours -- fundamental to performance
print('KNN')
print('Tuning k')
best_k = 1
best_acc = 0
for k in range(1,11,2): #1,3,5,7,9
     knn = KNeighborsClassifier(n_neighbors=k).fit(X_train,y_train) #k parameter to tune -- how many nearest points to compare to
     #print('predicting...')
     knn_preds = knn.predict(X_val)     
     accuracy = accuracy_score(y_val,knn_preds)
     print("{} {:.3f}".format(k,accuracy))
     if k==1:
          best_acc=accuracy
     elif accuracy - best_acc > 0.009: #update if accuracy is only changing at the third decimal place
          best_k = k
          best_acc = accuracy
     
          
print("Selected k {} gives acc of {:.3f} on val: ".format(best_k,best_acc))           
print("Training final decision tree")
knn = KNeighborsClassifier(n_neighbors=best_k).fit(X_train,y_train) #k parameter to tune -- how many nearest points to compare to (very slow to train)
knn_preds = knn.predict(X_test)
knn_confusion = confusion_matrix(y_test,knn_preds)
print("KNN confusion matrix:")
print(knn_confusion)
metric_report(y_test,knn_preds)


          

